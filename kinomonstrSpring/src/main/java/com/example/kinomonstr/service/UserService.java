package com.example.kinomonstr.service;

import com.example.kinomonstr.domain.Role;
import com.example.kinomonstr.domain.User;
import com.example.kinomonstr.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailSender mailSender;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username);
    }

    public boolean addUser(User user) {
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if (userFromDb != null) {
            return false;
        }
//        Integer activationCode = Integer.parseInt(RandomStringUtils.random(4,false,true));
        user.setActive(true);
        user.setRoles(Collections.singleton(Role.ADMIN));
        //user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setActivationCode(UUID.randomUUID().toString());

        userRepo.save(user);
        sendMessage(user);
        return true;
    }


    public void updateProfile(User user, String password, String email) {
        String userEmail = user.getEmail();

        boolean isEmailChanged = (email != null && !email.equals(userEmail)) ||
                (userEmail != null && !userEmail.equals(email));

        if (isEmailChanged) {
            user.setEmail(email);

            if (!StringUtils.isEmpty(email)) {
                user.setActivationCode(UUID.randomUUID().toString());
            }
        }

        if (!StringUtils.isEmpty(password)) {
            user.setPassword(password);
        }

        userRepo.save(user);

        if (isEmailChanged) {
            sendMessage(user);
        }
    }


    private void sendMessage(User user) {
        if (!StringUtils.isEmpty(user.getEmail())) {
            String message = String.format(
                    "Hello, %s! \n" +
                            "Welcome to Sweater. Please, visit next link: http://localhost:8080/activate/%s",
                    user.getUsername(),
                    user.getActivationCode()
            );
            mailSender.send(user.getEmail(), "Activation code", message);
        }
    }

    // проверка пользователя на ввод ключа активации
    public boolean activateUser(String code) {
        User user = userRepo.findByActivationCode(code);//ищем в бд пользователя по коду регистрации
        if (user == null) {//если пользователь не найден то возвращаем фолс
            return false;
        }

        user.setActivationCode(null);  //если пользователя перешол по ссылке, то делаем код активации = 0
        userRepo.save(user);

        return true;
    }


    //--------------------------------------------------------------PASSWORD-----------------------------
    public boolean updatePassword(String password, String email) {
        User user = userRepo.findByEmail(email);
        if (user == null) {
            return false;
        }
        // генерация рандомного кода
        //Integer activationCode = Integer.parseInt(RandomStringUtils.random(4,false,true));
        user.setActivationCode(UUID.randomUUID().toString());

        user.setPassword(password);
        userRepo.save(user);
        sendMessageForPassword(email);
        return true;
    }

    // Проверяет существует ли email.
    // ок - если существует и отправляется код активации на почту
//    public boolean checkEmailForPassword(String email) {
//        User user = userRepo.findByEmail(email);
//        if(user == null){
//            System.out.println("no exist in DB");
//            return false;
//        }else{
//            //Integer activationCode = Integer.parseInt(RandomStringUtils.random(4,false,true));
//            user.setActivationCode(UUID.randomUUID().toString());
//            userRepo.save(user);
//            sendMessageForPassword(email);
//            System.out.println("exist in DB");
//            return true;
//        }
//    }

    private void sendMessageForPassword(String email) {
        User user = userRepo.findByEmail(email);
        if (!StringUtils.isEmpty(email)) {    //если емаил не пустой, то отправляем ему сообщение с кодом активации
            String message = String.format(
                    "Hello, %s! \n" +
                            "Welcome. Please, vedite activation code for restore password- http://localhost:8080/activate/%s",
                    email,
                    user.getActivationCode()
            );

            mailSender.send(user.getEmail(), "Activation code", message);
        }
    }
}

    //----------------------------------------------------------------------------------
    //ОТПРАВКА СООБЩЕНИЕ С КОДОМ АКТИВАЦИИ НА ПОЧТУ ДЛЯ ВОССТАНОВЛЕНИЯ ПАРОЛЯ
//    private void sendMessage(User user) {
//        if (!StringUtils.isEmpty(user.getEmail())) {
//            String message = String.format(
//                    "Hello, %s! \n" +
//                            "Welcome to Sweater. Please, vedite activation code - %d",
//                    user.getUsername(),
//                    user.getActivationCode()
//            );
//
//            mailSender.send(user.getEmail(), "Activation code", message);
//        }
//    }


//    public void updateProfile(User user, String password, String email) {
//        int a = 1000; // Начальное значение диапазона - "от"
//        int b = 9999; // Конечное значение диапазона - "до"
//        int random_number1 = a + (int) (Math.random() * b);
//        String userEmail = user.getEmail();
//
//        boolean isEmailChanged = (email != null && !email.equals(userEmail)) ||
//                (userEmail != null && !userEmail.equals(email));
//
//        if (isEmailChanged) {
//            user.setEmail(email);
//
//            if (!StringUtils.isEmpty(email)) {
//                user.setActivationCode(random_number1);
//            }
//        }
//
//        if (!StringUtils.isEmpty(password)) {
//            user.setPassword(password);
//        }
//
//        userRepo.save(user);
//
//        // if (isEmailChanged) {
//        sendMessage(user);
//        // }
//    }


//    public void addNewPassword(String email){
//        Optional<User> optional = userRepo.findByEmail(email);
//        User user = optional.get();
//        user.setResetToken(UUID.randomUUID().toString());
//
//        // Save token to database
//        userRepo.save(user);
//        sendMessageForPassword(email);
//        return true;
//    }

//    public boolean addNewPassword(String email){
//        Optional<User> optional = userRepo.findByEmail(email);
//        User user = optional.get();
//        user.setResetToken(UUID.randomUUID().toString());
//
//        // Save token to database
//        userRepo.save(user);
//        sendMessageForPassword(email);
//        return true;
//    }
//
//    public void updatePassword( Map<String, String> requestParams) {
//        // Find the user associated with the reset token
//        User user = userRepo.findByResetToken(requestParams.get("token"));
//
//        // This should always be non-null but we check just in case
//
//            User resetUser = user;
//
//            // Set new password
//            resetUser.setPassword(requestParams.get("password"));
//
//            // Set the reset token to null so it cannot be used again
//            resetUser.setResetToken(null);
//
//            // Save user
//            userRepo.save(resetUser);
//    }

//    public void sendMessageForPassword(String email) {
//        if (!StringUtils.isEmpty(email)){
//            // Lookup user in database by e-mail
//            Optional<User> optional = userRepo.findByEmail(email);
//            User user = optional.get();
//
//            String message = String.format(
//                    "Hello, %s! \n" +
//                            "To reset your password, click the link below: http://localhost:8080/resetToken/%s",
//                    email,
//                    user.getResetToken()
//            );
//            mailSender.send(user.getEmail(), "Reset password code", message);
//        }
//    }




//-----------------------------------------------------------------------------------------------------
    //восстановление пароля

//    public void sendMessageForPassword(String  userEmail, HttpServletRequest request) {
//        // Lookup user in database by e-mail
//        Optional<User> optional = userRepo.findUserByEmail(userEmail);
//        // Generate random 36-character string token for reset password
//        User user = optional.get();
//        user.setResetToken(UUID.randomUUID().toString());
//
//        // Save token to database
//        userRepo.save(user);
//
//        sendMessageForPassword(user,request);
//    }

//    public void replaceNewPassword(String password, Map<String,String> requestParams) {
//        // Find the user associated with the reset token
//        Optional<User> user = userRepo.findByResetToken(requestParams.get("token"));
//
//        // This should always be non-null but we check just in case
//        if (user.isPresent()) {
//
//            User resetUser = user.get();
//
//            // Set new password
//            resetUser.setPassword(password);
//
//            // Set the reset token to null so it cannot be used again
//            resetUser.setResetToken(null);
//
//            // Save user
//            userRepo.save(resetUser);
//        }
//    }
//    public void replaceNewPassword(User user, String password) {
//       // if (!StringUtils.isEmpty(password)) {
//        User emailFromDB = userRepo.findByEmail(user.getEmail());
//        if(emailFromDB!=null){
//            user.setUsername("qwer");
//        }
//       // User emailFromDB2 = userRepo.findByEmail(user.setPassword(password));
//        user.setPassword(password);
//          //  user.setPassword(password);
//       // }
//        userRepo.save(user);
//
////        sendMessage(user);
//    }
//
//    //отправка сообщения на почту чтобы восстановить пароль
//    public boolean sendMessageForReplacePassword(User user){
//        User emailFromDB = userRepo.findByEmail(user.getEmail());
//
//        if (emailFromDB == null || StringUtils.isEmpty(emailFromDB)) {
//            return false;
//        }
//        //иначе сообщение отправляется пользователю
//        sendMessageForPassword(user);
//        return true;
//    }

    //------------------------------------------------------------------------------------------------


    //отправка сообщение на почту для восстановления пароля
//    private void sendMessageForPassword(User user,  HttpServletRequest request) {
//        String appUrl = request.getScheme() + "://" + request.getServerName();
//        if (!StringUtils.isEmpty(user.getEmail())) {
//            String message = String.format(
//                    "To reset your password, click the link below:\n" + appUrl
//                            + "/reset?token=" + user.getResetToken());
////                    "Hello, %s! \n" +
////                            "Welcome to films. Please, visit next link: http://localhost:8080/replacePassword",
////                    user.getUsername()
//
//            mailSender.send(user.getEmail(), "Activation code", message);
//        }
//    }



//    public boolean activateUserForPassword(String code) {
//        User user = userRepo.findByResetToken(code);//ищем в бд пользователя по коду регистрации
//        if (user == null) {//если пользователь не найден то возвращаем фолс
//            return false;
//        }
//
//        user.setResetToken(null);  //если пользователя перешол по ссылке, то делаем код активации = 0
//        userRepo.save(user);
//
//        return true;
//    }





    //--------------------------------------------------------------------------------------------------

//    public boolean addUser(User user) {
//        User userFromDb = userRepo.findByUsername(user.getUsername());
//        if (userFromDb != null) {
//            return false;
//        }
//        user.setActive(true);
//        user.setRoles(Collections.singleton(Role.USER));
//        user.setActivationCode(UUID.randomUUID().toString());
//        userRepo.save(user);
//        sendMessage(user);
//        return true;
//    }
//
//
//
//    private void sendMessage(User user) {
//        if (!StringUtils.isEmpty(user.getEmail())) {
//            String message = String.format(
//                    "Hello, %s! \n" +
//                            "Welcome to Sweater. Please, visit next link: http://localhost:8080/activate/%s",
//                    user.getUsername(),
//                    user.getActivationCode()
//            );
//
//            mailSender.send(user.getEmail(), "Activation code", message);
//        }
//    }
//
//    public boolean activateUser(String code) {
//        User user = userRepo.findByActivationCode(code);
//
//        if (user == null) {
//            return false;
//        }
//
//        user.setActivationCode(null);
//
//        userRepo.save(user);
//
//        return true;
//    }


//
//    private void sendMessage(User user) {
//        if (!StringUtils.isEmpty(user.getEmail())) {
//            String message = String.format(
//                    "Hello, %s! \n" +
//                            "Welcome to Sweater. Please, vedite activation code - %d",
//                    user.getUsername(),
//                    user.getActivationCode()
//            );
//
//            mailSender.send(user.getEmail(), "Activation code", message);
//        }
//    }


//    public void updateProfile(User user, String password, String email) {
//        int a = 1000; // Начальное значение диапазона - "от"
//        int b = 9999; // Конечное значение диапазона - "до"
//        int random_number1 = a + (int) (Math.random() * b);
//        String userEmail = user.getEmail();
//
//        boolean isEmailChanged = (email != null && !email.equals(userEmail)) ||
//                (userEmail != null && !userEmail.equals(email));
//
//        if (isEmailChanged) {
//            user.setEmail(email);
//
//            if (!StringUtils.isEmpty(email)) {
//                user.setActivationCode(random_number1);
//            }
//        }
//
//        if (!StringUtils.isEmpty(password)) {
//            user.setPassword(password);
//        }
//
//        userRepo.save(user);
//
//        // if (isEmailChanged) {
//        sendMessage(user);
//        // }
//    }

//------------------------------------------         ANDROID            -------------------------------
