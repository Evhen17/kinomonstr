package com.example.kinomonstr.repos;


import com.example.kinomonstr.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepo extends JpaRepository<User,Long> {
    User findByUsername(String username);
 //User findByActivationCode(int code);
   User findByActivationCode(String code);
   User findByEmail(String email);
  // Optional<User> findByEmail(String email);
   //User findByResetToken(String resetToken);


    //Optional<User> findById(String id);
}
