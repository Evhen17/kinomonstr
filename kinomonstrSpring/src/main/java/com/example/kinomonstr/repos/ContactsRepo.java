package com.example.kinomonstr.repos;

import com.example.kinomonstr.domain.Contacts;
import org.springframework.data.repository.CrudRepository;

public interface ContactsRepo extends CrudRepository<Contacts, Long>{
}
