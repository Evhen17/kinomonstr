package com.example.kinomonstr.repos;

import com.example.kinomonstr.domain.Feedback;
import org.springframework.data.repository.CrudRepository;

public interface FeedbackRepo extends CrudRepository<Feedback, Long> {
}
