package com.example.kinomonstr.controller;

import com.example.kinomonstr.domain.Contacts;
import com.example.kinomonstr.domain.User;
import com.example.kinomonstr.repos.ContactsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Controller
public class ContactController {
    @Value("${upload.path}")    //ищет upload.path в проперти и вставляет его в переменную uploadPath
    private String uploadPath;
    @Autowired
    private ContactsRepo contactsRepo;

    @GetMapping("/contacts")
    public String showContacts(Model model){
//        Iterable<Contacts> listContacts = contactsRepo.findAll();
//        listContacts = contactsRepo.findAll();
//        model.addAttribute("listContacts", listContacts);
        return "contacts";
    }

    @PostMapping("/contacts")
    public String addContacts(@AuthenticationPrincipal User user, @Valid Contacts contacts,
                              BindingResult bindingResult,
                              //список аргументов и ошибок валидации
                              Model model)
                              //@RequestParam("file") MultipartFile file)
           {
        contacts.setAuthor(user);
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("message", contacts);
        } else {
            //ЕСЛИ ФАЙЛ НЕ РАВНЯЕТСЯ = 0 ,ТО  МЫ ДОБАВЛЯЕМ ЕГО В КЛАСС
           // saveFile(contacts, file);
            model.addAttribute("message", null); //если валидация прошла успешно, то удаляем сообщ

            contactsRepo.save(contacts);            //СОХРАНЯЕТ ВСЕ В БАЗУ ДАННЫХ
            }

        Iterable<Contacts> listContacts = contactsRepo.findAll();;
        model.addAttribute("listContacts", listContacts);

        return "contacts";
    }

//    private void saveFile(@Valid Contacts message, @RequestParam("file") MultipartFile file) throws IOException {
//        if (file != null && !file.getOriginalFilename().isEmpty()) {
//            File uploadDir = new File(uploadPath);
//
//            if (!uploadDir.exists()) {
//                uploadDir.mkdir();
//            }
//
//            String uuidFile = UUID.randomUUID().toString();
//            String resultFilename = uuidFile + "." + file.getOriginalFilename();
//
//            file.transferTo(new File(uploadPath + "/" + resultFilename));
//
//            message.setFilename(resultFilename);
//        }
//    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/allcontacts")
    public String allContacts(Model model){
        Iterable<Contacts> listContacts = contactsRepo.findAll();
        listContacts = contactsRepo.findAll();
        model.addAttribute("listContacts", listContacts);

        return "allcontacts";
    }
}
