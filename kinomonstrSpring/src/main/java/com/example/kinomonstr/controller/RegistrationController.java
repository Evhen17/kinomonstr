package com.example.kinomonstr.controller;

import com.example.kinomonstr.domain.User;
import com.example.kinomonstr.repos.UserRepo;
import com.example.kinomonstr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class RegistrationController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepo userRepo;

    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = userService.activateUser(code);
        if (isActivated) {
            model.addAttribute("message", "User successfully activated");
        } else {
            model.addAttribute("message", "Activation code is not found!");
        }
        return "login";
    }

    @GetMapping("/registration")
    public String userRegistration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@Valid User user, BindingResult bindingResult, Model model) {
//        if (user.getPassword() != null && !user.getPassword().equals(user.getPassword2())) {
//            model.addAttribute("passwordError", "Passwords are different!");
//        }

        if (bindingResult.hasErrors()) {
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errors);

            return "registration";
        }

        //если не смогли добавтить пользователя в БД, то значит что он существует
        if (!userService.addUser(user)) {
            model.addAttribute("message", "User exists!");
            return "registration";
        }
        return "redirect:/login";
    }

    @GetMapping("/replacePassword")
    public String getNewPassword() {
        return "newPassword";
    }

    @PostMapping("/replacePassword")
    public String updateNewPassword(
            @RequestParam String password, @RequestParam String email, Map<String,Object> model
    ) {
        if (!userService.updatePassword(password, email)) {
            model.put("message", "User not exists!");
            return "newPassword";
        }
        return "redirect:/login";
    }


    //------------------------------            ANDRIOD          ------------------------
//    @GetMapping("/activationKod")
//    public String activate(){
//        return "activationKod";
//    }
//
//      //-----------------------------------       АНДРОИД       ------------------------------------
//    @PostMapping("/activationKod")
//    public String activate(int code, Model model){
//        User user = userRepo.findByActivationCode(code);
//        if(user.getActivationCode()==code){
//            model.addAttribute("message", "Пользователь удачно активировал код");
//            user.setActive(true);
//            userRepo.save(user);
//        }
//        else {
//            model.addAttribute("message", "Код активациии не был найден");
//        }
//        return "redirect:/login";
//    }
}

































//    @GetMapping("/resetToken/{code}")
//    public String activateForPassword(@PathVariable String code) {
//       boolean isActivated = userService.activateUserForPassword(code);
////
////        if (isActivated) {
////            model.addAttribute("message", "User successfully activated");
////        } else {
////            model.addAttribute("message", "Activation code is not found!");
////        }
//        return "newPassword";
//    }

    //запрос с отображением емаила по которому пользователя хочет восстановить пароль
//    @GetMapping("/restorePasswordWithEmail")
//    public String restorePasswordWithEmail(){
//        return "restorePasswordWithEmail";
//    }

    //отправка кода активации на емаил пользователя
//    @PostMapping("/restorePasswordWithEmail")
//    public String postRestorePasswordWithEmail(HttpServletRequest request, Model model, String email){
//        //если не смогли добавтить пользователя в БД, то значит что он существует
//        userService.sendMessageForPassword(email, request);
//        return "redirect:/replacePassword";
//    }
    // Process form submission from forgotPassword page
//    @RequestMapping(value = "/restorePasswordWithEmail", method = RequestMethod.POST)
//    public String processForgotPasswordForm(@RequestParam("email") String userEmail) {
//
//        userService.addNewPassword(userEmail);
//
//        return "redirect:/replacePassword";
//
//    }
    //отображается страница с измененим пароля(установлением нового)
//    @GetMapping("/replacePassword")
//    public String replacePassword(){
//        return "newPassword";
//    }
// Display form to reset password
//    @RequestMapping(value = "/replacePassword", method = RequestMethod.GET)
//    public String displayResetPasswordPage(@RequestParam("token") String token) {
//
//            User user = userService.findUserByResetToken(token);
//
////        Optional<User> user = userService.findUserByResetToken(token);
////
////            model.addAttribute("resetToken", token);
//
//        return "newPassword";
//    }
//    //восстановление пароля
//    @PostMapping("/replacePassword")
//    public String postReplacePassword(
//            @RequestParam Map<String,String> requestParams,
//            @RequestParam String password
////            @RequestParam String email
//    ) {
//        userService.replaceNewPassword(password, requestParams);
//        return "redirect:/login";
//    }
// Process reset password form
//@RequestMapping(value = "/replacePassword", method = RequestMethod.POST)
//public String  setNewPassword(@RequestParam Map<String, String> requestParams) {
//
//    userService.updatePassword(requestParams);
//
//    return "redirect:/login";
//}
    // Going to reset page without a token redirects to login page
//    @ExceptionHandler(MissingServletRequestParameterException.class)
//    public String handleMissingParamss(MissingServletRequestParameterException ex) {
//        return "redirect:/login";
//    }






    //------------------------------            ANDRIOD          ------------------------
//    @GetMapping("/activationKod")
//    public String activate(){
//        return "activationKod";
//    }

    //  -----------------------------------       АНДРОИД       ------------------------------------
//    @PostMapping("/activationKod")
//    public String activate(User user, int code, Model model){
//        if(user.getActivationCode()==code){
//            model.addAttribute("message", "Пользователь удачно активировал код");
//            user.setActive(true);
//            userRepo.save(user);
//        }
//        else {
//            model.addAttribute("message", "Код активациии не был найден");
//        }
//        return "redirect:/login";
//    }

