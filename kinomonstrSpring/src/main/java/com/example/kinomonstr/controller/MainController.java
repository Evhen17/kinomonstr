package com.example.kinomonstr.controller;

import com.example.kinomonstr.domain.Feedback;
import com.example.kinomonstr.domain.User;
import com.example.kinomonstr.repos.FeedbackRepo;
import com.example.kinomonstr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;


@Controller
public class MainController {
    @Autowired
    private UserService userService;
    @Autowired
    private FeedbackRepo feedbackRepo;

    @GetMapping("/")
    public String showMainPage(Model model){
        return "mainPage";
    }

    @PostMapping("/filmsMorgenPage")
    public String addFeedback(@AuthenticationPrincipal User user, @Valid Feedback message,
                              BindingResult bindingResult,  //список аргументов и ошибок валидации
                              Model model){

        //выводит список ошибок
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("message", message);
        } else {
            message.setAuthor(user);
            model.addAttribute("message", null);
            feedbackRepo.save(message);
        }

        Iterable<Feedback> messages = feedbackRepo.findAll();  //В БАЗЕ ДАННЫХ НАХОДЯТСЯ ВСЕ НОВОСТИ, КОТОРЫЕ БЫЛИ ВВЕДЕНЫ РАНЬШЕ

        model.addAttribute("messages", messages);         //ВЕРНЕТ СПИСОК НОВОСТЕЙ КОТОРЫЕ ЛЕЖАТ В БАЗЕ ДАННЫХ
        //1-ЫМ ШАГОМ - МЫ СОХРАНИЛИ ВВЕДЕННОЕ НАМИ СООБЩЕНИЕ
        //2-ЫМ ВЗЯЛИ ИЗ РЕПОЗИТОРИЯ, ПОЛОЖИЛИ В МОДЕЛЬ И ВЕРНУЛИ ПОЛЬЗОВАТЕЛЮ

        return "/filmsMorgenPage";
    }

    @GetMapping("/filmsMorgenPage")
    public String showFilmsMorgenPage(Model model) {
        Iterable<Feedback> messages = feedbackRepo.findAll();;

        messages = feedbackRepo.findAll();

        model.addAttribute("messages", messages);

        return "filmsMorgenPage";
    }

    @GetMapping("/profile")
    public String getProfile(Model model, @AuthenticationPrincipal User user) {//model - будем ложить данные, в ней мы ожидаем имя пользователя и емаил.
        model.addAttribute("username", user.getUsername());                 //@AuthenticationPrincipal User user - ожидает пользователя из контекста
        model.addAttribute("email", user.getEmail());

        return "profile";
    }

    @PostMapping("profile")
    public String updateProfile(
            @AuthenticationPrincipal User user,
            @RequestParam String password,
            @RequestParam String email
    ) {
        userService.updateProfile(user, password, email);

        return "redirect:/profile";
    }


}
