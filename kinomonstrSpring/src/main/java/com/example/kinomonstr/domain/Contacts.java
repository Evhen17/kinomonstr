package com.example.kinomonstr.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Contacts{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "vfhuigfyui")
    @Length(max = 2048, message = "svdsyjjyg")
    private String film;
//
    @NotBlank(message = "Заполните название кинотеатра")
    @Length(max = 2048, message = "Message too long (more than 2kB)")
    private String cinema;
//
    @NotBlank(message = "Заполните отзыв")
    @Length(max = 2048, message = "Message too long (more than 2kB)")
    private String otzuv;

   // private String filename;

    @ManyToOne(fetch = FetchType.EAGER) //одному пользователю(user) будет соответствовать много сообщений
    @JoinColumn(name = "user_id")
    private User author;

    public Contacts() {
    }

    public Contacts(String film, String cinema, String otzuv, User author) {
        this.film = film;
        this.cinema = cinema;
        this.otzuv = otzuv;
        this.author = author;
    }

//
//    public Contacts(String film, User author) {
//        this.film = film;
//        this.author = author;
//    }


//    public String getFilename() {
//        return filename;
//    }
//
//    public void setFilename(String filename) {
//        this.filename = filename;
//    }

    public String getAuthorName(){
        return author!=null ? author.getUsername():"<none>";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilm() {
        return film;
    }

    public void setFilm(String film) {
        this.film = film;
    }

    public String getCinema() {
        return cinema;
    }

    public void setCinema(String cinema) {
        this.cinema = cinema;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getOtzuv() {
        return otzuv;
    }

    public void setOtzuv(String otzuv) {
        this.otzuv = otzuv;
    }
}
