package com.example.kinomonstr.config;

import com.example.kinomonstr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
//@EnableOAuth2Sso
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;
//для авториизации через гугл
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .mvcMatchers("/").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .csrf().disable();
//    }
//
//для авториизации через гугл
//    @Bean
//    public PrincipalExtractor principalExtractor(UserRepo userDetailsRepo) {
//        return map -> {
//            String id = (String) map.get("sub");
//
//            User user = userDetailsRepo.findById(id).orElseGet(() -> {
//                User newUser = new User();
//
//                newUser.setId(id);
//                newUser.setUsername((String) map.get("name"));
//                newUser.setEmail((String) map.get("email"));
//                newUser.setGender((String) map.get("gender"));
//                newUser.setUserpic((String) map.get("picture"));
//
//                return newUser;
//            });
//
//            return userDetailsRepo.save(user);
//        };
//    }

//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @Bean
//    public PasswordEncoder getPasswordEncoder() {
//        return new BCryptPasswordEncoder(4);
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userService)
//                .passwordEncoder(passwordEncoder);
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {  //СИСТЕМА ЗАХОДИТ В http И ПЕРЕДАЕТ НА ВХОД ОБЬЕКТ,
        //  В КОТОРОМ МЫ ВКЛЮЧАЕМ: авторизацию, указываем что для этого пути - "/" , мы всем надаем полный доступ
        http
                .authorizeRequests()
                .antMatchers("/login","/restorePasswordWithEmail","/replacePassword","/activationKod", "/activate/*","/", "/registration", "/filmsMorgenPage", "/static/**", "/static/*", "/style.css", "/static", "/static/morg.jpg", "/contacts").permitAll()
                //для всех остальных url-путей требуется авторизация
                .anyRequest().authenticated()
                .and()
                //включаем форму ЛОГИН, указываем что она находится по этому меппингу - "/login" , и разрешаем доступ всем
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                //включаем logout и разрешаем им пользоваться всем
                .logout()
                .permitAll();
//                .and()
//                .csrf().disable();  //для авторизации через гуггл
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService)  //userDetailsService(userService) - нужен для того  чтобы менеджер мог ходить в базу данных
                // и искать пользователей и их роли
                .passwordEncoder(NoOpPasswordEncoder.getInstance());    //ШИФРУЕТ ПАРОЛИ, ЧТОБЫ ОНИ НЕ ХРАНИЛИСЬ В ЯВНОМ ВИДЕ
//                .usersByUsernameQuery("select username, password, active from usr where username=?")
//                .authoritiesByUsernameQuery("select u.username, ur.roles from usr u inner join user_role ur on u.id = ur.user_id where u.username=?");
    }
}
