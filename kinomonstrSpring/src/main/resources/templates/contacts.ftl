<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->


<#--<div class="content">-->
    <#--<h1>Отзыв</h1>-->
    <#--Оставьте Ваш отзыв о просмотренном фильме-->
    <#--<div class="send_otzuv">-->
        <#--<form methd="post" action="/contacts">-->
            <#--<input type="text" placeholder="Фильм">-->
            <#--&lt;#&ndash;<input type="text" name="review_name"placeholder="Кинотеатр">&ndash;&gt;-->
            <#--&lt;#&ndash;<textarea name="review_text" placeholder="Отзыв"></textarea>&ndash;&gt;-->
            <#--<input type="submit" class="contact_button" value="Отправить">-->
        <#--</form>-->
    <#--</div>-->
        <#--<br>-->
        <#--<form method="get" action="/allcontacts">-->
            <#--<input type="submit" class="login_button" value="Отзывы">-->
        <#--</form>-->


    <#--<h1>Отзывы пользователей</h1>-->

    <#--<#list messages as message>-->
        <#--<div class="reviews">-->
            <#--&lt;#&ndash;<div class="review_name">&ndash;&gt;-->
                <#--${message.authorName}-->
            <#--&lt;#&ndash;</div>&ndash;&gt;-->
            <#--<div class="review_text">-->
                <#--${message.film}-->
                <#--&lt;#&ndash;${message.cinema}&ndash;&gt;-->
                <#--&lt;#&ndash;${message.otzuv}&ndash;&gt;-->
            <#--</div>-->

        <#--</div>-->
    <#--<#else>-->
        <#--Нет отзывов-->
    <#--</#list>-->
<#--</div>-->



<div class="content">
    <#--<h2>Отзывы</h2>-->
    <#--<#list listContacts as contacts>-->
        <#--<div class="reviews">-->
            <#--<div class="review_name">-->
                <#--${contacts.authorName}-->
            <#--</div>-->
            <#--<div class="review_text">-->
                <#--Фильм - <i>${contacts.film}</i>-->
                <#--<br>-->
                <#--Кинотеатр - <b>${contacts.cinema}</b>-->
                <#--<br>-->
                <#--Отзывы - ${contacts.otzuv}-->
            <#--</div>-->
        <#--</div>-->
    <#--<#else>-->
        <#--No message-->
    <#--</#list>-->

    <div class="send">
        <form method="post">
            <#--<input type="text" name="film" placeholder="Введите фильм" />-->
            <#--<input type="text" name="cinema" placeholder="Введите кинотеатр" />-->
            <#--<input type="text" name="otzuv" placeholder="Введите отзыв" />-->
            <input type="text" name="film" class="{(textError??)?string('is-invalid', '')" value="<#if contacts??>${contacts.film}</#if>" placeholder="Введите фильм" />
            <#if textError??>
                ${textError}
            </#if>
            <input type="text" name="cinema" class="{(textError??)?string('is-invalid', '')" value="<#if contacts??>${contacts.cinema}</#if>" placeholder="Введите кинотеатр" />
            <#if textError??>
                ${textError}
            </#if>
            <input type="text" name="otzuv" class="{(textError??)?string('is-invalid', '')" value="<#if contacts??>${contacts.otzuv}</#if>" placeholder="Введите отзыв" />
            <#if textError??>
                ${textError}
            </#if>
           <#--<input type="file" name="file"/>-->
            <input type="hidden" name="_csrf" value="${_csrf.token}" />
            <input type="submit" value="Добавить">
        </form>

        <form method="get" action="/allcontacts">
             <input type="submit" class="login_button" value="Отзывы">
        </form>
    </div>
</div>

</@shablon.mainpage>