<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->
<div class="content">
    <h2>Отзывы о фильмах</h2>

    <#list listContacts as contacts>
        <div class="reviews">
            <div class="review_name">
                ${contacts.authorName}
            </div>
            <div class="review_text">
                Фильм - <i>${contacts.film}</i>
                <br>
                Кинотеатр - <b>${contacts.cinema}</b>
                <br>
                Отзывы - ${contacts.otzuv}
                <#--<div>-->
                    <#--<#if contacts.filename??>-->
                        <#--<img src="/img/${contacts.filename}">-->
                    <#--</#if>-->
                <#--</div>-->
            </div>
        </div>
    <#else>
        No message
    </#list>

</div>
</@shablon.mainpage>