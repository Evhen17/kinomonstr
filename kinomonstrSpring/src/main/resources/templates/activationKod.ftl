<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->

    ${message?ifExists}
    <div class="content">
        <h1>Код активации</h1>
        Введите код который Вам пришел
        <div class="send_otzuv">
            <form method="post" action="/activationKod">
                <input type="text" name="code" placeholder="Введите код">
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <input type="submit" class="login_button" value="Отправить">
            </form>
        </div>
    </div>
</@shablon.mainpage>
