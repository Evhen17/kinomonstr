<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->

${message?ifExists}

<div class="content">
    <h1>Профиль</h1>
    Введите данные, которые Вы хотите изменить!
    <div class="send_otzuv">
        <form method="post" action="/profile">
            <#--<input type="text" name="username" placeholder="Введите имя">-->
            <input type="email" name="email" placeholder="some@gmail.com">
            <input type="password" class="login_password" name="password" placeholder="Введите пароль">
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <input type="submit" class="regis_button" value="Изменить">
        </form>
    </div>
</div>


</@shablon.mainpage>