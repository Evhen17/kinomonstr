<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->

    <div class="content">
        <h1>Восстановление пароля</h1>
        Введите свою почту чтобы восстановить пароль
        <div class="send_otzuv">
            <form method="post" action="/restorePasswordWithEmail">
                <input type="email" name="email" placeholder="some@gmail.com">
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <input type="submit" class="login_button" value="Вост пароль">
            </form>
        </div>
    </div>


</@shablon.mainpage>