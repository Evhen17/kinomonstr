<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->
    ${message?ifExists}

    <div class="content">
        <h1>Новые фильмы</h1>
        <div class="films_block">
            <a href="/filmsMorgenPage"><img src="morg.jpg"></a>
            <a href="#"><img src="max.png"></a>
            <a href="#"><img src="cloud.jpg"></a>
            <a href="#"><img src="dead.png"></a>
        </div>
        <h1>Новые сериалы</h1>
        <div class="films_block">
            <a href="#"><img src="inter.jpg"></a>
            <a href="#"><img src="breakk.png"></a>
            <a href="#"><img src="matrix.png"></a>
            <a href="#"><img src="silicon.png"></a>
        </div>

        <div class="posts">
            <hr>
            <h2><a href="#">Как снимали Интерстеллар</a></h2>
            <div class="posts_content">
                <p>
                    45 лет исполнилось Кристоферу Нолану — одному из самых успешных режиссеров нашего времени, чьи работы одинаково любимы как взыскательными критиками, так и простыми зрителями. Фильмом изначально занималась студия Paramount. Когда Кристофер Нолан занял место режиссера, студия Warner Bros., которая выпускала его последние фильмы, добилась участия в проекте.
                </p>
            </div>

            <p><a href="#">Читать</a></p>
            <hr>
            <h2><a href="#">Актер Том Хенкс поделился впечатлением о фестивале</a></h2>

            <div class="posts_content">
                <p>16 февраля в Лондоне состоялась 67-я церемония вручения наград Британской киноакадемии. Леонардо ДиКаприо, Брэд Питт, Анджелина Джоли, Кейт Бланшетт, Хелен Миррен, Эми Адамс, Кристиан Бэйл, Альфонсо Куарон и другие гости и победители премии — в нашем репортаже.</p>
            </div>
            <p><a href="#">Читать</a></p>

        </div>
    </div>

</@shablon.mainpage>

<#--<div class="main">-->
    <#--<div class="header">-->
        <#--<div class="logo">-->
            <#--<div class="logo_text">-->
                <#--<!--это div - для подписи "Кино наша страсть"&ndash;&gt;-->
                <#--<h1><a href="/">КиноМоностр</a></h1>-->
                <#--<h2>Кино - наша страсть!</h2>-->
            <#--</div>-->
        <#--</div>-->

        <#--<div class="menubar">-->
            <#--<ul class="menu">-->
                <#--<li class="selected"><a href="/">Главная</a></li>-->
                <#--<li><a href="/filmsMorgenPage">Фильмы</a></li>-->
                <#--<li><a href="#">Рейтинг фильмов</a></li>-->
                <#--<li><a href="/contacts">Контакты</a></li>-->
                <#--<li><a href="/registration">Регистрация</a></li>-->
                <#--<li><a href="/login">Вход</a></li>-->
            <#--</ul>-->
        <#--</div>-->
    <#--</div>-->

    <#--<div class="site_content">-->
        <#--<div class="sidebar_container"><!--этот класс отвечает за правую часть блоков, где будет находится ВХОД, ПОИСК и тд&ndash;&gt;-->
            <#--<div class="sidebar">-->
                <#--<h2>Поиск</h2>-->
                <#--<form method="post" action="#" id="search_form">-->
                    <#--<input type="search" name="search_field" placeholder="Ваш запрос"/>-->
                    <#--<input type="submit" class="btn" value="Найти"/>-->
                <#--</form>-->
            <#--</div>-->

            <#--<div class="enter">-->
                <#--<h2>Вход</h2>-->
                <#--<form method="post" action="/login">-->
                    <#--<input type="text" name="username" placeholder="Логин">-->
                    <#--<input type="password" name="password" placeholder="Пароль">-->
                    <#--<input type="hidden" name="_csrf" value="${_csrf.token}"/>-->
                    <#--<input type="submit" class="enter_btn" name="enter_button" value="Войти">-->
                <#--</form>-->
                <#--<div class="lables_passreg_text">-->
                    <#--<span><a href="#">Забыли пароль?</a></span> | <span><a href="/registration">Регистрация</a></span>-->
                <#--</div>-->

                <#--<div>-->
                    <#--<form action="/logout" method="post">-->
                        <#--<input type="hidden" name="_csrf" value="${_csrf.token}"/>-->
                        <#--<input type="submit" class="enter_btn" value="Выйти"/>-->
                    <#--</form>-->
                <#--</div>-->
            <#--</div>-->

            <#--<div class="news">-->
                <#--<h2>Новости</h2>-->
                <#--<span>28.02.2019</span>-->
                <#--<p>Мы запустили расширенный поиск</p>-->
                <#--<div class="lables_passreg_text_2">-->
                    <#--<a href="#">Читать</a>-->
                <#--</div>-->
            <#--</div>-->
            <#--<div class="raiting">-->
                <#--<h2>Рейтинг фильмов</h2>-->
                <#--<ul>-->
                    <#--<li><a href="#">Интерстеддар</a><span class="raiting_sidebar">8.1</span></li>-->
                    <#--<li><a href="#">Матрица</a><span class="raiting_sidebar">7.7</li>-->
                    <#--<li><a href="#">Безумный Макс</a><span class="raiting_sidebar">9.3</li>-->
                    <#--<li><a href="#">Облачный атлас</a><span class="raiting_sidebar">8.5</li>-->
                <#--</ul>-->
            <#--</div>-->
        <#--</div>-->

        <#--<div class="content">-->
            <#--<h1>Новые фильмы</h1>-->
            <#--<div class="films_block">-->
                <#--<a href="/filmsMorgenPage"><img src="morg.jpg"></a>-->
                <#--<a href="#"><img src="max.png"></a>-->
                <#--<a href="#"><img src="cloud.jpg"></a>-->
                <#--<a href="#"><img src="dead.png"></a>-->
            <#--</div>-->
            <#--<h1>Новые сериалы</h1>-->
            <#--<div class="films_block">-->
                <#--<a href="#"><img src="inter.jpg"></a>-->
                <#--<a href="#"><img src="breakk.png"></a>-->
                <#--<a href="#"><img src="matrix.png"></a>-->
                <#--<a href="#"><img src="silicon.png"></a>-->
            <#--</div>-->

            <#--<div class="posts">-->
                <#--<hr>-->
                <#--<h2><a href="#">Как снимали Интерстеллар</a></h2>-->
                <#--<div class="posts_content">-->
                    <#--<p>-->
                        <#--45 лет исполнилось Кристоферу Нолану — одному из самых успешных режиссеров нашего времени, чьи работы одинаково любимы как взыскательными критиками, так и простыми зрителями. Фильмом изначально занималась студия Paramount. Когда Кристофер Нолан занял место режиссера, студия Warner Bros., которая выпускала его последние фильмы, добилась участия в проекте.-->
                    <#--</p>-->
                <#--</div>-->

                <#--<p><a href="#">Читать</a></p>-->
                <#--<hr>-->
                <#--<h2><a href="#">Актер Том Хенкс поделился впечатлением о фестивале</a></h2>-->

                <#--<div class="posts_content">-->
                    <#--<p>16 февраля в Лондоне состоялась 67-я церемония вручения наград Британской киноакадемии. Леонардо ДиКаприо, Брэд Питт, Анджелина Джоли, Кейт Бланшетт, Хелен Миррен, Эми Адамс, Кристиан Бэйл, Альфонсо Куарон и другие гости и победители премии — в нашем репортаже.</p>-->
                <#--</div>-->
                <#--<p><a href="#">Читать</a></p>-->

            <#--</div>-->
        <#--</div>-->
    <#--</div>-->

    <#--<div class="footer">-->
        <#--<p>-->
            <#--<a href="#">Главная</a> |-->
            <#--<a href="#">Фильмы</a> |-->
            <#--<a href="#">Сериалы</a> |-->
            <#--<a href="#">Рейтинг фильмов</a> |-->
            <#--<a href="#">Контакты</a> |-->
        <#--</p>-->
        <#--<p>wh-db.com 2019</p>-->
    <#--</div>-->
<#--</div>-->