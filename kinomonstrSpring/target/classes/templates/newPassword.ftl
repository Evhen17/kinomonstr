<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->


    ${message?ifExists}
    <div class="content">
        <h1>Восстановление пароля</h1>
        Введите новый пароль
        <div class="send_otzuv">
            <form method="post" action="/replacePassword">
                <input type="password" class="login_password" name="password" placeholder="Введите пароль">
                <input type="email" name="email" placeholder="some@gmail.com">
                <#--<input type="password" class="login_password" name="password" placeholder="Повторите пароль">-->
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <input type="submit" class="regis_button" value="Сменить пароль">
            </form>
        </div>
    </div>


</@shablon.mainpage>
