<#macro mainpage>   <#--макрос с название page -->
    <#include "security.ftl">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kinomonstr</title>
    <link rel="stylesheet" href="style.css">
    <meta name="description" content="Кономонстр - портал о кино">
    <meta name="keywords" content="фильмы, фильмы онлайн, hd">
</head>

<body>
<div class="main">
    <div class="header">
        <div class="logo">
            <div class="logo_text">
                <!--это div - для подписи "Кино наша страсть"-->
                <h1><a href="/">КиноМонстр</a></h1>
                <h2>Кино - наша страсть!</h2>
            </div>
        </div>

        <div class="menubar">
            <ul class="menu">
                <li class="selected"><a href="/">Главная</a></li>
                <li><a href="/filmsMorgenPage">Фильмы</a></li>
                <li><a href="/profile">Профиль</a></li>
                <li><a href="/contacts">Отзыв</a></li>
                <li><a href="/registration">Регистрация</a></li>
                <li><a href="/login">Вход</a></li>
            </ul>
        </div>
    </div>

    <div class="site_content">
        <div class="sidebar_container"><!--этот класс отвечает за правую часть блоков, где будет находится ВХОД, ПОИСК и тд-->
            <div class="sidebar">
                <h2>Поиск</h2>
                <form method="post" action="#" id="search_form">
                    <input type="search" name="search_field" placeholder="Ваш запрос"/>
                    <input type="submit" class="btn" value="Найти"/>
                </form>
            </div>

            <div class="enter">
                <h2>Вход</h2>
                <div>Имя пользователя - <b>${name}</b></div>
                <form method="post" action="/login">
                    <input type="text" name="username" placeholder="Логин">
                    <input type="password" name="password" placeholder="Пароль">
                    <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                    <input type="submit" class="enter_btn" name="enter_button" value="Войти">
                </form>
                <div class="lables_passreg_text">
                    <span><a href="#">Забыли пароль?</a></span> | <span><a href="/registration">Регистрация</a></span>
                </div>

                <div>
                    <form action="/logout" method="post">
                        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                        <input type="submit" class="enter_btn" value="Выйти"/>
                    </form>
                </div>
            </div>

            <div class="news">
                <h2>Новости</h2>
                <span>28.02.2019</span>
                <p>Мы запустили расширенный поиск</p>
                <div class="lables_passreg_text_2">
                    <a href="#">Читать</a>
                </div>
            </div>
            <div class="raiting">
                <h2>Рейтинг фильмов</h2>
                <ul>
                    <li><a href="#">Интерстеддар</a><span class="raiting_sidebar">8.1</span></li>
                    <li><a href="#">Матрица</a><span class="raiting_sidebar">7.7</li>
                    <li><a href="#">Безумный Макс</a><span class="raiting_sidebar">9.3</li>
                    <li><a href="#">Облачный атлас</a><span class="raiting_sidebar">8.5</li>
                </ul>
            </div>
        </div>

        <#nested>

    </div>

    <div class="footer">
        <p>
            <a href="#">Главная</a> |
            <a href="#">Фильмы</a> |
            <a href="#">Сериалы</a> |
            <a href="#">Рейтинг фильмов</a> |
            <a href="#">Отзыв</a> |
        </p>
        <p>wh-db.com 2019</p>
    </div>
</div>
</body>
</html>
</#macro>

