<#macro login path isRegisterForm>
<form action="${path}" method="post">
    <div class="content">
    <#if isRegisterForm>
        <h1>Регистрация</h1>
        Пройдите регистрацию, ради того чтобы получить все преимущества пользования сайтом!
    </#if>
    <#if !isRegisterForm>
        <h1>Вход</h1>
        Ввойдите в аккаунт для продолжения пользования сайтом.
    </#if>
        <div class="send_otzuv">
                <div><input type="text" name="username" value="<#if user??>${user.username}</#if>" class="{(usernameError??)?string('is-invalid', '')" placeholder="Введите имя">
                <#if usernameError??>
                    ${usernameError}
                </#if></div>

                <div><#if isRegisterForm>
                    <input type="email" name="email" value="<#if user??>${user.email}</#if>" class="{(emailError??)?string('is-invalid', '')"  placeholder="some@gmail.com">
                    <#if emailError??>
                        ${emailError}
                    </#if>
                </#if></div>

                <div><input type="password" class="login_password" class="{(passwordError??)?string('is-invalid', '')"  name="password" placeholder="Введите пароль">
                <#if passwordError??>
                    ${passwordError}
                </#if></div>

                <#--<div>-->
                <#--<#if isRegisterForm>-->
                <#--<input type="password" class="login_password" name="password2" class="{(password2Error??)?string('is-invalid', '')"  placeholder="Повторите пароль">-->
                <#--<#if password2Error??>-->
                    <#--${password2Error}-->
                <#--</#if></#if></div>-->

                <input type="hidden" name="_csrf" value="${_csrf.token}"/>

                <input type="submit" class="regis_button" <#if isRegisterForm>Зарегистрироваться<#else>Войти</#if>>
        </div>
    </div>
</form>
</#macro>