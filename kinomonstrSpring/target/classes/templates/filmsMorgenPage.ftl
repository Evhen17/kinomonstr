<#import "parts/shablonPage.ftl" as shablon/>  <#--для использования шаблона нужно его экспортировать и назначим ему алиас - с-->
<@shablon.mainpage>       <#--при использовании макросов мы используем собаку @  -->

<div class="content">
            <h1>Моргенштерн</h1>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/a4bL6EQTYJw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            <div class="info_film_page">
                <span class="label">Рейтинг: </span>
                <span class="value">9.9/10 </span>
                <span class="label">Год: </span>
                <span class="value">2019</span>
                <span class="label">Режисер: </span>
                <span class="value">Алишер Тагирович</span>
            </div>
            <hr>

            <h2>Описание</h2>
            <div class="description_film">
                <img src="morg.jpg">
                Моргеншторн рееееее реккк раааааааа раввв ыууууууупа вппппппп вппппппппп рвааааааааа арввввв риввввввввввввпа ымввввввввввв мыввввввввв вымммммм вмыыыыыыыыы выммммммммммм ывмммммммм выммммммммммм выам вмыыыыыыыыыы вмыыыыыы вымммммммммммвы выпм
            </div>
            <hr>

            <h2>Отзывы о Моргенштерне</h2>

            <#list messages as message>
            <div class="reviews">
                <div class="review_name">
                    ${message.authorName}
                </div>
                <div class="review_text">
                     ${message.text}
                </div>
            </div>
            <#else>
            No message
            </#list>

            <div class="send">
                <form method="post">
                    <input type="text" name="text" class="{(textError??)?string('is-invalid', '')" value="<#if message??>${message.text}</#if>" placeholder="Введите сообщение" />
                    <#if textError??>
                            ${textError}
                    </#if>
                    <input type="hidden" name="_csrf" value="${_csrf.token}" />
                    <input type="submit" value="Добавить">
                </form>
            </div>
        </div>


</@shablon.mainpage>